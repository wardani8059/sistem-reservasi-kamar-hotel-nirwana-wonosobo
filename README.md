# Sistem Reservasi Kamar Hotel Nirwana Wonosobo

Suatu sistem untuk menangani reservasi kamar Hotel Nirwana Wonosobo secara online. Sistem ini dimaksudkan untuk memudahkan klien dalam melakukan reservasi kamar hotel dan memudahkan resepsionis dalam mengelola data reservasi klien.

## Utama

Repository ini berisikan dokumen analisis dan perancangan untuk website sistem reservasi kamar Hotel Nirwana Wonosobo. 

Berikut merupakan daftar analisis yang dikerjakan:
1. Dokumen Software Requirement Specification (SRS)
2. Dokumen Use Case:
- Use Case
- Use Case Description
- Use Case Scenario
3. Dokumen Diagram BPMN:
- BPMN reservasi untuk sistem terkini
- BPMN reservasi untuk sistem yang dikerjakan
4. Dokumen Activity Diagram:
- Login
- Reservasi kamar
- Cek data reservasi
- Update data reservasi
5. Analisis Perancangan UI
6. Dokumen Sequence Diagram:
- Sequence diagram admin
- Sequence diagram klien
7. Dokumen Class Diagram
8. Dokumen Pengujian (Metode SUS)
9. UCP Software Cost Estimate (untuk menentukan estimasi biaya berdasar UCP)

## Hasil

Desain prototipe yang dihasilkan dirancang dari analisis yang telah dilakukan sebelumnya. Sehingga, prototipe yang dibuat sesuai dengan kebutuhan user.

Prototipe dari aplikasi dapat diakses di link berikut:
- Sisi klien: https://www.figma.com/proto/HoPlB1aSM7FHOfo42XNJ5m/APPL-Hotel-Reservation?node-id=2%3A2&scaling=scale-down&page-id=0%3A1&starting-point-node-id=66%3A424
- Sisi admin: https://www.figma.com/proto/xHSoYpA3sI7l4sirOvMxtW/Untitled?node-id=2%3A24&scaling=scale-down&page-id=0%3A1&starting-point-node-id=2%3A24 

